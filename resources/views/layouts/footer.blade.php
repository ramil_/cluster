<style>
    #footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        /* Set the fixed height of the footer here */
        height: 60px;
        background-color: #f5f5f5;
    }
</style>
<footer id="footer">
    <div class="container">
        <p class="text-muted">© Сидоров Роман.</p>
    </div>
</footer>
