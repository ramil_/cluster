<div class="panel panel-default">

    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-center">Управление блоками страницы</h3>
        </div>
    </div>

    <div class="panel-body">

        <div id="main_blocks_wrap">

            <page-blocks-list :page="{{ $page }}"></page-blocks-list>

        </div>

    </div>
</div>
