<?php

return [

    'block_title'       => 'Контакты',
    'primary_documents' => 'Первичная документация',
    'application'       => 'Заявка <br> на сотрудничество',
    'btn_title'         => 'Стать партнером',
];
