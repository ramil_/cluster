<?php

return [

    'logout'      => 'Выйти',
    'login'       => 'Войти',
    'main'        => 'Главная',
    'catalog'     => 'Каталог',
    'cooperation' => 'Сотрудничество',
    'assortment'  => 'Ассортимент',
    'promotions'  => 'Акции',
    'delivery'    => 'Доставка',
    'posts'       => 'Журнал',
    'contacts'    => 'Контакты',
    'about'       => 'О компании',
    'partner_btn' => 'Стать партнером',
];











