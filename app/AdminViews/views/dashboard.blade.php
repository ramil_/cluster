<div class="box box-success">
    <div class="box-header with-border">

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h3 class="box-title">Вы вошли как - {{ $user->name }}</h3>
            </div>
        </div>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
        </div>


    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <!-- /.col -->
            <div class="col-md-12 col-sm-12" style="max-height: 400px; overflow: auto">


            </div>
            <!-- /.col -->
            <div class="col-md-12 col-sm-12">
                <div  style="border-bottom: 2px solid #000;"></div>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
</div>