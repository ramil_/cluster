<?php

namespace App\Models\Pages;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Pages\Home
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pages\Home newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pages\Home newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pages\Home query()
 */
class Home extends Model
{
    //
}
